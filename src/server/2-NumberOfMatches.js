const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");
let result = {};

function matchesWonByTeam() {

    const object = {}
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {

        if (datarow.season in object) {
            
            if (datarow.winner in object[datarow.season]) {
                object[datarow.season]
                [datarow.winner] += 1
            } else {
                object[datarow.season]
                [datarow.season] = 1
            }


        } else {
            object[datarow.season] = {
                [datarow.winner]: 1
            }
        }



    })
        .on("end", () => {
            fs.writeFile(
                path.join(__dirname, "../public/output/2-NumberofMatches.json"),
                JSON.stringify(object),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )

        })
}

matchesWonByTeam();