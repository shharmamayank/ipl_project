const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");
// let result = {};

function dismissalsbymost() {
    // const object = {}
    fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', (datarow) => {
        deliveriesData.push(datarow)
    }).on('end', () => {

        let dismissalsofOnePlayerToAnother = Object.fromEntries(Object.entries(deliveriesData.reduce((object, datarow) => {
            if (datarow.player_dismissed != "" && datarow.dismissal_kind !== "run out" && datarow.dismissal_kind !== "retired hurt") {
                let player = datarow.player_dismissed
                let bowler = datarow.bowler
                if (object[player]) {
                    if (object[player][bowler]) {
                        object[player][bowler] += 1
                    }
                    else {
                        object[player][bowler] = 1
                    }
                }
                else {
                    object[player] = {}
                    object[player][bowler] = 1
                }
            }
            return object
        }, {})).map(each => {
            let player = each[0]
            let maxValue = Math.max(...(Object.values(each[1])))
            let dismissals = Object.fromEntries(Object.entries(each[1]).filter(each => {
                return each[1] === maxValue
            }))
            return [player, dismissals]
        }))

        const result = dismissalsbymost
        let res = dismissalsbymost(deliveriesData)
        console.log(res)

    })

    fs.writeFile(
        path.join(__dirname, "../public/output/8-one-player-dissmisal-to-other.json"),
        JSON.stringify(res),\
        (error) => {
            if (error !== null) {
                console.log(error);
            }
        }
    )

}


res();

