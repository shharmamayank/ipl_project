const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");

function wonPlayerOfTheMatchMost() {
    let result = {}
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {
        if (result[datarow.season]) {
            if (result[datarow.season][datarow.player_of_match]) {
                result[datarow.season][datarow.player_of_match] += 1
            }
            else {
                result[datarow.season][datarow.player_of_match] = 1
            }
        }
        else {
            result[datarow.season] = {}
            result[datarow.season][datarow.player_of_match] = 1
        }
    })
        .on("end", () => {
            let object = {}
            Object.entries(result).forEach((year)=>{
                let value= Math.max(...Object.values(result[year[0]]))
                object[year[0]]=[];
                Object.entries(year[1]).forEach((result))
            })
            fs.writeFile(
                path.join(__dirname, "../public/output/6-won-player-of-the-match-most.json"),
                JSON.stringify(result),
                (error) => {
                    if (error !== null) {
                        
                        console.log(error);
                    }
                }
            )

        })
}
wonPlayerOfTheMatchMost();
