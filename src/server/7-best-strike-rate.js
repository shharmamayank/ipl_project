const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");
let result = {};

function bestStrikeRate() {
    const object = {}
    fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {
       if (!object.includes(datarow.season)) {
            object.push(datarow.season)
        }
        console.log(object)
    let strikeRateOfBatsmanEachSeason = Object.entries(deliveriesJson.reduce((object, datarow) => {
        let array = matchesJson.filter(each => {
            return (each.id === datarow.match_id)
        })
        let season = array[0].season
        if (acc[datarow.batsman]) {
            if (object[datarow.batsman][season]) {
                object[datarow.batsman][season]["runsScored"] += parseInt(datarow.batsman_runs)
                object[datarow.batsman][season]["ballsFaced"] += 1
            }
            else {
                object[datarow.batsman][season] = {}
                object[datarow.batsman][season]["runsScored"] = parseInt(datarow.batsman_runs)
                object[datarow.batsman][season]["ballsFaced"] = 1
            }
        } else {
            object[datarow.batsman] = {}
            object[datarow.batsman][season] = {}
            object[datarow.batsman][season]["runsScored"] = parseInt(datarow.batsman_runs)
            object[datarow.batsman][season]["ballsFaced"] = 1
        }
        let player = datarow[0]
        let calculateStrikeRate = years.reduce((object1, year) => {
            if (datarow[1][year]) {
                object[year] = Number(((datarow[1][year]["runsScored"] / datarow[1][year]["ballsFaced"]) * 100).toFixed(2))
            }
            acc[player] = calculateStrikeRate
        })
        .on("end", () => {
            fs.writeFile(
                path.join(__dirname, "../public/output/7-best-strike-rate.json"),
                JSON.stringify(object),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )

        }
        )
        1
        
    


bestStrikeRate()


