const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");

function topEconomicalBowlers(year) {
    let result = [];
    let deliveriesData = [];
    let MatchData = [];
    // let economy=[]

    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', (datarow) => {
        // if (datarow.season === String(year)) {
        //     if (Start === undefined) {
        //         Start = +datarow.id;
        //     } else {
        let object = {}
        //         End = +datarow.id;
        //     }
        // }
        result.push(datarow)
    }).on('end', () => {

        fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', (datarow) => {
            deliveriesData.push(datarow)
        }).on('end', () => {
            let matches = result.filter(index => {
                
                return index.season === "2015"
            }).map(index => index.id)
            let totalRuns = deliveriesData.reduce((object, datarow) => {
                if (matches.includes(datarow.match_id)) {
                    object[datarow.bowler] ? object[datarow.bowler] += parseInt(datarow.total_runs) : object[datarow.bowler] = parseInt(datarow.total_runs)
                }

            }, {})

            let totalBall = deliveriesData.reduce((object, datarow) => {
                if (matches.includes(datarow.match_id)) {
                    if (object[curr.bowler]) {
                        (object.wide_runs == 0 && object.noball_runs == 0) ? object[datarow.bowler] += 1 : object[datarow.bowler] += 0
                    }
                    else {
                        (datarow.wide_runs == 0 && datarow.noball_runs == 0) ? object[datarow.bowler] = 1 : object[datarow.bowler] = 0
                    }
                }
            }, {})
            let economy = Object.entries(deliveriesData.reduce((object, datarow) => {
                if (matches.includes(datarow.match_id)) {
                    object[datarow.bowler] = Number((totalRuns[datarow.bowler] / (totalBall[datarow.bowler] / 6)).toFixed(2))
                }
            }, {}))
            let resultList = Object.fromEntries(Object.entries(economy).sort((a, b) => a[1].economy - b[1].economy).slice(0, 10).filter(bowler => {
                bowler[1] = parseFloat(bowler[1].economy)
                return true
            }))
            return resultList
        })

        const topBowler = resultList
        //     if (a[1] < b[1]) return -1
        //     else if (a[1] > b[1]) return 1
        //     else return 0
        // })

        //let resultList = Object.fromEntries(economy.slice(0, 10))
    })
    fs.writeFile(
        path.join(__dirname, "../public/output/4-top-economical-bowlers.json"),
        JSON.stringify(topBowler),
        (error) => {
            if (error !== null) {
                console.log(error);
            }
        }
    )
}





// topEconomicalBowlers()