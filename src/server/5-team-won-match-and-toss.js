const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");
let result = {};

function matchesWonByTeam() {
    const object = {}
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {
        if (datarow.toss_winner === datarow.winner) {
            (object[datarow.winner]) ? object[datarow.winner] += 1 : object[datarow.winner] = 1
        }

    })
        .on("end", () => {
            fs.writeFile(
                path.join(__dirname, "../public/output/5-team-won-match-and-toss.json"),
                JSON.stringify(object),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                        
                    }
                }
            )

        })
}


matchesWonByTeam();
