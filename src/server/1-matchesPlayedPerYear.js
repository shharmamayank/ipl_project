
const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");
let result = {};

function matchesPlayedPerYear() 
{
    
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {
        if (datarow.season in result) {
            result[datarow.season] += 1;
        } else {
            result[datarow.season] = 1;
        }
    })
        .on("end", () => {
            fs.writeFile(
                path.join(__dirname, "../public/output/1-matchesPlayedPerYear.json"),
                JSON.stringify(result),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )

        })
}

matchesPlayedPerYear();