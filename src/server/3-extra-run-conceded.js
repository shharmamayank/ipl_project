const fs = require("fs");
const path = require("path");
const { parse } = require("csv-parse");

function extraRunConceded(year) {
    let extraRunConceded = {}
    let start;
    let end;
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(parse({ columns: true })).on('data', function (datarow) {
        if (datarow.season === String(year)) {
            if (start === undefined) {
                start = +datarow.id;
            } else {
                end = +datarow.id
            }
        }
    })
        .on("end", () => {

            fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(parse({ columns: true })).on('data', function (datarowDelivery) {

                if (datarowDelivery.match_id >= start && datarowDelivery.match_id <= end) {
                    if (datarowDelivery.bowling_team in extraRunConceded) {
                        extraRunConceded[datarowDelivery.bowling_team] += parseInt(datarowDelivery.extra_runs)
                    } else {
                        extraRunConceded[datarowDelivery.bowling_team] = parseInt(datarowDelivery.extra_runs)
                    }
                }
            })
                .on("end", () => {
                    fs.writeFile(
                        path.join(__dirname, "../public/output/3-extra-run-conceded.json"),
                        JSON.stringify(extraRunConceded),
                        (error) => {
                            
                            if (error !== null) {
                                console.log(error);
                            }
                        }
                    )

                })

        })
}

extraRunConceded(2016)